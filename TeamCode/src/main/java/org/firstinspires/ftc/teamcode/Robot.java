package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;

/**
 * Created by ifwel on 10/23/2017.
 */

public abstract class Robot extends LinearOpMode {
    public ElapsedTime timer = new ElapsedTime();
    DriveTrain dt;
    JewelBars jb;
    Lifts lifts;
    Claws claws;
    ColorSensors cs;
    PhoneCamera pc;
    IntakeSystems is;

    int cameraMonitorViewId;

    public void initialize(){
        DcMotor w1 = hardwareMap.dcMotor.get("w1");
        DcMotor w2 = hardwareMap.dcMotor.get("w2");
        DcMotor w3 = hardwareMap.dcMotor.get("w3");
        DcMotor w4 = hardwareMap.dcMotor.get("w4");

        w1.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        w2.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        w3.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        w4.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);

        dt = new DriveTrain(w1, w2, w3, w4);

        Servo dropRight = hardwareMap.servo.get("dropRight");
        Servo dropLeft = hardwareMap.servo.get("dropLeft");
        Servo rotationRight = hardwareMap.servo.get("rotationRight");
        Servo rotationLeft = hardwareMap.servo.get("rotationLeft");

        jb = new JewelBars(dropRight, dropLeft, rotationRight, rotationLeft);

        DcMotor glyphLift = hardwareMap.dcMotor.get("glyphLift");
        DcMotor relicLift = hardwareMap.dcMotor.get("relicLift");

        glyphLift.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        glyphLift.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        relicLift.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        relicLift.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        lifts = new Lifts(glyphLift, relicLift);

        Servo glyphClaw = hardwareMap.servo.get("glyphClaw");
        Servo relicClaw = hardwareMap.servo.get("relicClaw");

        claws = new Claws(glyphClaw, relicClaw);

        ColorSensor rightSensor = hardwareMap.colorSensor.get("rightSensor");
        ColorSensor leftSensor = hardwareMap.colorSensor.get("leftSensor");

        cs = new ColorSensors(rightSensor , leftSensor);

        cameraMonitorViewId = hardwareMap.appContext.getResources().getIdentifier("cameraMonitorViewId", "id", hardwareMap.appContext.getPackageName());
        pc = new PhoneCamera(cameraMonitorViewId);

        /*DcMotor glyphIntake = hardwareMap.dcMotor.get("glyphIntake");

        is = new IntakeSystems(glyphIntake);*/
    }

    public void knockOffJewel(String ourColor){
        double change = .15;
        if(ourColor.equals("red")){
            if(cs.getRightColor().equals("red")){
                jb.moveRotationRight(change);
            }
            else if(cs.getRightColor().equals("blue")){
                change *= -1;
                jb.moveRotationRight(change);
            }
        }
        else if(ourColor.equals("blue")){
            if(cs.getLeftColor().equals("red")){
                change *= 2;
                jb.moveRotationLeft(change);
            }
            else if(cs.getLeftColor().equals("blue")){
                change *= -2;
                jb.moveRotationLeft(change);
            }
        }

        timer.reset();
        while(timer.seconds() < 1){

        }

        if(ourColor.equals("red")){
            if(change == .15){
                jb.moveRotationRight(-.1);
            }
            else if(change == -.15){
                jb.moveRotationRight(.1);
            }
        }
        else if(ourColor.equals("blue")){
            if(change == .15){
                jb.moveRotationLeft(-.2);
            }
            else if(change == -.15){
                jb.moveRotationLeft(.2);
            }
        }

        timer.reset();
        while(timer.seconds() < 1){

        }
    }

    public void addTelemetry(String caption, double value){
        telemetry.addData(caption, value);
        telemetry.update();
    }
}
