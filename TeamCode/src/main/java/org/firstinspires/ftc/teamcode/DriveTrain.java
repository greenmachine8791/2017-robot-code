package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.util.ElapsedTime;

/**
 * Created by ifwel on 10/23/2017.
 */

public class DriveTrain {
    ElapsedTime timer = new ElapsedTime();
    public final int COUNTS_PER_REVOLUTION = 1440;
    public final double WHEEL_DIAMETER_INCHES = 4;
    public final double INCHES_PER_REVOLUTION = 3.141 * WHEEL_DIAMETER_INCHES * 1.5;
    public final double COUNTS_PER_INCH = COUNTS_PER_REVOLUTION/INCHES_PER_REVOLUTION;
    public final int DELTA = 20;

    public DcMotor w1;
    public DcMotor w2;
    public DcMotor w3;
    public DcMotor w4;

    private double wheelPower = .4;

    public DriveTrain(DcMotor w1, DcMotor w2, DcMotor w3, DcMotor w4){
        this.w1 = w1;
        this.w2 = w2;
        this.w3 = w3;
        this.w4 = w4;
        w1.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        w2.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        w3.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        w4.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        w1.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        w2.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        w3.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        w4.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        w2.setDirection(DcMotorSimple.Direction.REVERSE);
        w3.setDirection(DcMotorSimple.Direction.REVERSE);
    }

    public void driveFoward(double inches){
        int counts = inchesToCounts(inches);
        drive(counts, counts, counts, counts);
    }

    public void driveBackward(double inches){
        int counts = inchesToCounts(-inches);
        drive(counts, counts, counts, counts);
    }

    public void driveLeft(double inches){
        int counts = inchesToCounts(inches);
        drive(counts, -counts, -counts, counts);
    }

    public void driveRight(double inches) {
        int counts = inchesToCounts(inches);
        drive(-counts, counts, counts, -counts);
    }

    public void drive(int p1, int p2, int p3, int p4){
        drive(p1, p2, p3, p4, wheelPower);
    }

    public void drive(int p1, int p2, int p3, int p4, double power){
        w1.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        w2.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        w3.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        w4.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        w1.setTargetPosition(p1);
        w2.setTargetPosition(p2);
        w3.setTargetPosition(p3);
        w4.setTargetPosition(p4);
        w1.setPower(power);
        w2.setPower(power);
        w3.setPower(power);
        w4.setPower(power);
        timer.reset();
        while (!(Math.abs(w1.getCurrentPosition() - p1) < DELTA)){

        }
        w1.setPower(0);
        w2.setPower(0);
        w3.setPower(0);
        w4.setPower(0);
    }

    public void setWheelPower(double wheelPower){
        this.wheelPower = wheelPower;
    }

    public void resetEncoders(){
        w1.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        w2.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        w3.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        w4.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        w1.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        w2.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        w3.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        w4.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        w2.setDirection(DcMotorSimple.Direction.REVERSE);
        w3.setDirection(DcMotorSimple.Direction.REVERSE);
}

    public int inchesToCounts(double inches){
        return (int)(inches * COUNTS_PER_INCH);
    }

    public double countsToInches(int counts){
        return counts / COUNTS_PER_INCH;
    }
}
