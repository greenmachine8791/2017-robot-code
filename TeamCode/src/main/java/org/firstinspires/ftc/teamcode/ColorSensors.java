package org.firstinspires.ftc.teamcode;

import android.graphics.Color;

import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.I2cAddr;

/**
 * Created by ifwel on 11/1/2017.
 */

public class ColorSensors {

    public ColorSensor rightSensor;
    public ColorSensor leftSensor;

    public float[] hsvRight = {0f, 0f, 0f};
    public float[] hsvLeft = {0f, 0f, 0f};

    public ColorSensors(ColorSensor rightSensor, ColorSensor leftSensor){
        this.rightSensor = rightSensor;
        this.leftSensor = leftSensor;

        rightSensor.enableLed(true);
        leftSensor.enableLed(true);
    }

    public String getRightColor(){
        String color = "unknown";

        Color.RGBToHSV(rightSensor.red() * 8, rightSensor.green() * 8, rightSensor.blue() * 8, hsvRight);

        if(hsvRight[0] >= 340 || hsvRight[0] <= 20){
            color = "red";
        }
        else if(hsvRight[0] >= 180 && hsvRight[0] <= 260){
            color = "blue";
        }

        return color;
    }

    public String getLeftColor(){
        String color = "unknown";

        Color.RGBToHSV(leftSensor.red() * 8, leftSensor.green() * 8, leftSensor.blue() * 8, hsvLeft);

        if(hsvLeft[0] >= 340 || hsvLeft[0] <= 20){
            color = "red";
        }
        else if(hsvLeft[0] >= 180 && hsvLeft[0] <= 260){
            color = "blue";
        }

        return color;
    }

    public void setAddresses(String color){
        if(color.equals("red")){
            rightSensor.setI2cAddress(I2cAddr.create8bit(0x3c));
            leftSensor.setI2cAddress(I2cAddr.create8bit(0x30));
        }
        else if(color.equals("blue")){
            leftSensor.setI2cAddress(I2cAddr.create8bit(0x30));
            rightSensor.setI2cAddress(I2cAddr.create8bit(0x3c));
        }
    }
}
