package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.util.ElapsedTime;

/**
 * Created by ifwel on 12/16/2017.
 */

@Autonomous(name="Just Jewel Blue")
public class AutoJewelBlue extends Robot{

    ElapsedTime timer = new ElapsedTime();

    public void runOpMode(){
        initialize();

        waitForStart();

        jb.dropLeft();

        timer.reset();

        while(timer.seconds() < 4){
            telemetry.addData("Color: ", cs.getLeftColor());
            telemetry.addData("Hue: ", cs.hsvLeft[0]);
            telemetry.addData("Red: ", cs.leftSensor.red());
            telemetry.addData("Blue: ", cs.leftSensor.blue());
            telemetry.update();
        }

        knockOffJewel("blue");

        jb.raiseLeft();
    }
}
