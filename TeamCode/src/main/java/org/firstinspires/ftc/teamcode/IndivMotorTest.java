package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;

/**
 * Created by ifwel on 2/8/2018.
 */

@TeleOp(name="IndivMotorTest")
public class IndivMotorTest extends OpMode{

    public DcMotor motor;

    public void init(){
        motor = hardwareMap.dcMotor.get("motor");
        motor.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
    }

    public void loop(){
        motor.setPower(gamepad1.left_stick_y);
    }
}
