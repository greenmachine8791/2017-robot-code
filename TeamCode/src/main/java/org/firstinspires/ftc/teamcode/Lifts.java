package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.util.ElapsedTime;

/**
 * Created by ifwel on 11/8/2017.
 */

public class Lifts{
    public ElapsedTime timer = new ElapsedTime();

    public DcMotor glyphLift;

    public final double GLYPH_LIFT_GEAR_DIAMETER_INCHES = 1.135;
    public final int GLYPH_LIFT_COUNTS_PER_REVOLUTION = 140;
    public final double GLYPH_LIFT_INCHES_PER_REVOLUTION = (3.141 * GLYPH_LIFT_GEAR_DIAMETER_INCHES / 4) * 1.36;
    public final double GLYPH_LIFT_COUNTS_PER_INCH = GLYPH_LIFT_COUNTS_PER_REVOLUTION/GLYPH_LIFT_INCHES_PER_REVOLUTION;
    public final int DELTA = 5;

    public double glyphLiftPower = .5;
    private int glyphLiftPosition = 1;

    public Lifts(DcMotor glyphLift, DcMotor relicLift){
        this.glyphLift = glyphLift;
        glyphLift.setDirection(DcMotorSimple.Direction.REVERSE);
    }

    public void glyphLiftToOneInch(){
        int counts = (int)glyphLiftInchesToCounts(2);
        glyphLiftPosition = 1;
        glyphLift.setTargetPosition(counts);
        glyphLift.setPower(glyphLiftPower);
        timer.reset();
        while(timer.milliseconds() < 500){

        }
        while(glyphLift.isBusy()){

        }
        glyphLift.setPower(0);
    }

    public void glyphLiftToPos1(){
        int counts = 0;
        glyphLiftPosition = 1;
        glyphLift.setTargetPosition(counts);
        glyphLift.setPower(glyphLiftPower);
        timer.reset();
        while(timer.milliseconds() < 500){

        }
        while(glyphLift.isBusy()){

        }
        glyphLift.setPower(0);
    }

    public void glyphLiftToPos2(){
        int counts = (int)glyphLiftInchesToCounts(7);
        glyphLiftPosition = 2;
        glyphLift.setTargetPosition(counts);
        glyphLift.setPower(glyphLiftPower);
        timer.reset();
        while(timer.milliseconds() < 500){

        }
        while(glyphLift.isBusy()){

        }
        glyphLift.setPower(0);
    }

    public void glyphLiftToPos3(){
        int counts = (int)glyphLiftInchesToCounts(12.9);
        glyphLiftPosition = 3;
        glyphLift.setTargetPosition(counts);
        glyphLift.setPower(glyphLiftPower);
        timer.reset();
        while(timer.milliseconds() < 500){

        }
        while(glyphLift.isBusy()){

        }
        glyphLift.setPower(0);
    }

    public void relicLiftUp(double seconds){
        timer.reset();
        while (timer.seconds() < seconds){

        }
    }

    public void stopGlyphLift(){
        glyphLift.setTargetPosition(glyphLift.getCurrentPosition());
        glyphLift.setPower(0);
    }

    public int glyphLiftInchesToCounts(double inches){
        return (int)(inches * GLYPH_LIFT_COUNTS_PER_INCH);
    }

    public double glyphLiftCountsToInches(int counts){
        return counts / GLYPH_LIFT_COUNTS_PER_INCH;
    }

    public void setGlyphLiftPower(double glyphLiftPower){
        this.glyphLiftPower = glyphLiftPower;
    }

    public int getGlyphLiftPosition(){
        return glyphLiftPosition;
    }
// The scissor lift will be using one motor to control the
// scissoring and then another motor is mounted alongside that to control the other axis
    //In addition to the glyph lifts we have the X-Rail "Lift" extends to the left side of the robot.
    //and the Scissor lift which pushes the X-Rail lift up to get the relic with the most distance.
    //Trial and error most likely with the X-Rail by pulling a string.
    //Scissor Lift pushes the X-Rail Up
}
