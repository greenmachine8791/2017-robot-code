/* Copyright (c) 2017 FIRST. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted (subject to the limitations in the disclaimer below) provided that
 * the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list
 * of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of FIRST nor the names of its contributors may be used to endorse or
 * promote products derived from this software without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY THIS
 * LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.util.ElapsedTime;
import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.navigation.RelicRecoveryVuMark;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackable;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackables;

/**
 * Abstract Linear Autonomous OpMode
 * Created by Michael Zook 19Nov2017 for the Greenwood FTC Robotics Club.
 * This class provides relicRobot status and methods typical to all Relic Game autonomous OpModes
 *
 * Revised 01Dec2017 by Michael Zook */


public abstract class AutoLinearAbstract_VuForiaLimited extends LinearOpMode {

    /* =======================================================
     * CLASS MEMBERS (i.e., Class Status)
     * Common autonomous opmode members
     * ======================================================= */

    // Interfaces
    VuforiaLocalizer
            vuforia;    // Instance of the Vuforia localization engine

    // Objects
    ElapsedTime
            decryptTimer = new ElapsedTime(); // Pictograph decryption timer

    // Variables
    int
            decryptConfidenceScore = 0;   // 0=Low, 5=high

    boolean
            leftCryptoBoxColumn,    // true = left
            centerCryptoBoxColumn,  // true = cetner
            rightCryptoBoxColumn;   // true = right

    // Constants
    static final double
            MAXIMUM_DECRYPT_SECONDS = 5.0;  // Maximum time allowed to decrypt the pictograph

    static final int
            MINIMUM_DECRYPT_CONFIDENCE = 5; // Minimum number of consecutive decryption results



    /* =======================================================
     * CLASS METHODS (i.e., Class Behavior)
     * ======================================================= */

    /* -------------------------------------------------------
     * Method: runOpMode (Overridden Linear OpMode)
     * Purpose: Establish and initialize automated objects, and signal initialization completion
     * ------------------------------------------------------- */
    @Override
    public void runOpMode() {

        /* INITIALIZE ROBOT */

        // Add robot initialization code here before initializing VuForia


        // Initialize Vuforia Relic Pictograph Interpreter

        // Notify drive station that Vuforia is initializing
        telemetry.addLine("Initialize Vuforia");
        telemetry.update();

        // NOTE: The following code was pulled from the ConceptVuMarkIdentification.

        // Set the camera view to show the image on the robot controller
        int cameraMonitorViewId = hardwareMap.appContext.getResources().getIdentifier("cameraMonitorViewId", "id", hardwareMap.appContext.getPackageName());
        VuforiaLocalizer.Parameters parameters = new VuforiaLocalizer.Parameters(cameraMonitorViewId);

        // Identify the Vuforia license
        parameters.vuforiaLicenseKey = "Af3rFBP/////AAAAGYcAZEnsf0IHoahAzt2Kx754485oprEKRARHrK3KZqB0zHDDXteulg7rHYMeIA6X5ligYWE6nSxLr2ZpWrIVtPIu1JTBEHLoyCaS3H3A4DUgktaDS0jgPXs30YEauxKSSanRBxrJZBE7edVtmtpUnHayFn3cxaMHGNsBiQSCIfJa3hah4VzZceplmenNTybGj+UgpX5rFwRKzMIPCa/UYLRQpk9Mz1er3GSmw/vKMLXwMCbawiK3w3f/JZDrfbzAqdY3i98k8GMhb8a6JGzDivItWedF2sdxrv8j33deRhLv2goX+K9VkkNaCRG4G37c9GSUOBhpD8qAgFaCPcbxpVokQ1Aw7iUjkiW1ozgbXBKt";

        // Set the back camera of the robot controller to inspect the pictograph
        parameters.cameraDirection = VuforiaLocalizer.CameraDirection.BACK;
        this.vuforia = ClassFactory.createVuforiaLocalizer(parameters);

        // Load the data set containing the VuMarks for Relic Recovery - one trackable with three instances (left, center, right)
        VuforiaTrackables relicTrackables = this.vuforia.loadTrackablesFromAsset("RelicVuMark");
        VuforiaTrackable relicTemplate = relicTrackables.get(0);
        relicTemplate.setName("relicVuMarkTemplate");


        // Initialize pictograph results
        decryptConfidenceScore = 0;
        leftCryptoBoxColumn = centerCryptoBoxColumn = rightCryptoBoxColumn = false;


        // Report robot initialization complete
        telemetry.addLine("Initialization Complete");
        telemetry.addLine("Hold for Start");
        telemetry.update();


        // WAIT FOR GAME START (i.e., driver presses PLAY)
        waitForStart();


        // GAME STARTED - BEGIN AUTONOMOUS OPERATIONS

        // Enable pictograph inspection
        relicTrackables.activate();

        // Reset the decryption timer
        decryptTimer.reset();

        // Decrypt the pictograph. Run decryption until decryptTimer expires or until confidence is high.
        while (decryptTimer.seconds() < MAXIMUM_DECRYPT_SECONDS && decryptConfidenceScore < MINIMUM_DECRYPT_CONFIDENCE) {

            RelicRecoveryVuMark vuMark = RelicRecoveryVuMark.from(relicTemplate);  // Check for Relic Recovery VuMarks

            if (vuMark != RelicRecoveryVuMark.UNKNOWN) {   // If a VuNMark is found, capture the result and update confidence

                if (vuMark == RelicRecoveryVuMark.LEFT) {
                    if (!leftCryptoBoxColumn) { // If first time left VuMark instance found reset confidence and update result
                        decryptConfidenceScore = 0;
                        leftCryptoBoxColumn = true;
                        centerCryptoBoxColumn = false;
                        rightCryptoBoxColumn = false;
                    }
                    else decryptConfidenceScore++; // If left VuMark instance found again, increment confidence
                }

                if (vuMark == RelicRecoveryVuMark.CENTER){ // If first time center VuMark instance found reset confidence and update result
                    if (!centerCryptoBoxColumn) {
                        decryptConfidenceScore = 0;
                        leftCryptoBoxColumn = false;
                        centerCryptoBoxColumn = true;
                        rightCryptoBoxColumn = false;
                    }
                    else decryptConfidenceScore++; // If center VuMark instance found again, increment confidence
                }

                if (vuMark == RelicRecoveryVuMark.RIGHT){ // If first time right VuMark instance found reset confidence and update result
                    if (!rightCryptoBoxColumn) {
                        decryptConfidenceScore = 0;
                        leftCryptoBoxColumn = false;
                        centerCryptoBoxColumn = false;
                        rightCryptoBoxColumn = true;
                    }
                    else decryptConfidenceScore++; // If right VuMark instance found again, increment confidence
                }
            }
            else decryptConfidenceScore = 0; // If no VuMark instance found, reset confidence

            // Report decryption results (Note: These results are public and will be available in your custom autonomous app
            telemetry.addData("Left Column", leftCryptoBoxColumn);
            telemetry.addData("Center Column", centerCryptoBoxColumn);
            telemetry.addData("Right Column", rightCryptoBoxColumn);
            telemetry.update();
        }

        // Disable pictograph inspection
        relicTrackables.deactivate();

        // If other common autonomous operations are needed, add them here.
    }

}
