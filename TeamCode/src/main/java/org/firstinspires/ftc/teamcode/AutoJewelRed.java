package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.util.ElapsedTime;

/**
 * Created by ifwel on 12/16/2017.
 */

@Autonomous(name="Just Jewel Red")
public class AutoJewelRed extends Robot{

    ElapsedTime timer = new ElapsedTime();

    public void runOpMode(){
        initialize();

        waitForStart();

        jb.dropRight();

        timer.reset();

        while(timer.seconds() < 4){
            telemetry.addData("Color: ", cs.getRightColor());
            telemetry.addData("Hue: ", cs.hsvRight[0]);
            telemetry.addData("Red: ", cs.rightSensor.red());
            telemetry.addData("Blue: ", cs.rightSensor.blue());
            telemetry.update();
        }

        knockOffJewel("red");

        jb.raiseRight();
    }
}

