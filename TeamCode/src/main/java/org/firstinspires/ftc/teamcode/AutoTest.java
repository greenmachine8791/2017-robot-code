package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;

/**
 * Created by ifwel on 10/23/2017.
 */
public class AutoTest extends Robot{

    ElapsedTime timer = new ElapsedTime();
    public void runOpMode(){
        initialize();

        waitForStart();

        timer.reset();

        jb.dropRight();

        while(timer.seconds() < 15){
            telemetry.addData("Color: ", cs.getRightColor());
            telemetry.addData("Hue: ", cs.hsvRight[0]);
            telemetry.addData("Red: ", cs.rightSensor.red());
            telemetry.addData("Blue: ", cs.rightSensor.blue());
            telemetry.update();
        }

        knockOffJewel("red");
    }
}
