package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;

import java.lang.Math;

/**
 * Created by ifwel on 9/27/2017.
 */
@TeleOp(name="TestMecanum")
public class TestTeleop extends OpMode{

    public ElapsedTime timer = new ElapsedTime();

    public final double GLYPH_CLAW_CLOSE_POSITION = 0.4;
    public final double GLYPH_CLAW_OPEN_POSITION = 0.6;

    public double glyphClawPosition;

    public DcMotor w1;
    public DcMotor w2;
    public DcMotor w3;
    public DcMotor w4;

    public Servo dropRight;
    public Servo dropLeft;
    public Servo rotationRight;
    public Servo rotationLeft;

    public DcMotor glyphLift;

    public final double DR_START_POSITION = 0.3;
    public final double DL_START_POSITION = 0.65;
    public final double RR_START_POSITION = 0.6;
    public final double RL_START_POSITION = 0.7;

    public final double GLYPH_LIFT_GEAR_DIAMETER_INCHES = 1.135;
    public final int GLYPH_LIFT_COUNTS_PER_REVOLUTION = 140;
    public final double GLYPH_LIFT_INCHES_PER_REVOLUTION = (3.141 * GLYPH_LIFT_GEAR_DIAMETER_INCHES / 4) * 1.36;
    public final double GLYPH_LIFT_COUNTS_PER_INCH = GLYPH_LIFT_COUNTS_PER_REVOLUTION/GLYPH_LIFT_INCHES_PER_REVOLUTION;
    public final int DELTA = 5;

    public Servo glyphClaw;

    public boolean dropped = false;

    public boolean aPressed = false;

    public void init() {
        /*w1 = hardwareMap.dcMotor.get("w1");
        w2 = hardwareMap.dcMotor.get("w2");
        w3 = hardwareMap.dcMotor.get("w3");
        w4 = hardwareMap.dcMotor.get("w4");

        w1.setDirection(DcMotorSimple.Direction.REVERSE);
        w4.setDirection(DcMotorSimple.Direction.REVERSE);

        w1.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        w2.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        w3.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        w4.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        w1.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        w2.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        w3.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        w4.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        glyphClaw = hardwareMap.servo.get("glyphClaw");

        glyphClaw.setPosition(GLYPH_CLAW_OPEN_POSITION);
        glyphClawPosition = GLYPH_CLAW_OPEN_POSITION;*/

        /*dropRight = hardwareMap.servo.get("dropRight");
        dropLeft = hardwareMap.servo.get("dropLeft");
        rotationRight = hardwareMap.servo.get("rotationRight");
        rotationLeft = hardwareMap.servo.get("rotationLeft");

        dropRight.setPosition(DR_START_POSITION);
        dropLeft.setPosition(DL_START_POSITION);
        rotationRight.setPosition(RR_START_POSITION);
        rotationLeft.setPosition(RL_START_POSITION);*/

        glyphLift = hardwareMap.dcMotor.get("glyphLift");
    }

    public void loop(){
        if(!dropped){
            dropped = true;
            glyphLiftToPos2();
        }
        /*double x = -gamepad1.left_stick_x;
        double y = gamepad1.left_stick_y;
        double rotX = gamepad1.right_stick_x;
        boolean rotating = false;
        double radius = Math.sqrt((x * x) + (y * y));
        double angle = Math.abs(Math.toDegrees(Math.atan(y/x)));
        double varWheels = angle/45 - 1;
        int quadrant = 0;

        if(Math.abs(rotX) > 0.03){
            rotating = true;

            if(rotX > 0){
                setWheelPower(1, -1, 1, -1, rotX);
            }
            else{
                setWheelPower(-1, 1, -1, 1, -rotX);
            }
        }

        if(!rotating){
            if(Math.abs(x) < .03 && Math.abs(y) < .03){
                setWheelPower(0, 0, 0, 0, radius);
            }
            else if(angle >= 80){
                setWheelPower(y, y, y, y, 1);
            }
            else if(angle <= 10){
                if(x > 0){
                    setWheelPower(-1, 1, 1, -1, radius);
                }
                else if(x < 0){
                    setWheelPower(1, -1, -1, 1, radius);
                }
            }
            else{
                if(x > 0 && y > 0){
                    quadrant = 1;
                }
                else if(x < 0 && y > 0){
                    quadrant = 2;
                }
                else if(x < 0 && y < 0){
                    quadrant = 3;
                }
                else if(x > 0 && y < 0){
                    quadrant = 4;
                }

                if(quadrant == 0){
                    setWheelPower(0, 0, 0, 0, radius);
                }
                else if(quadrant == 1){
                    setWheelPower(varWheels, 1, 1, varWheels, radius);
                }
                else if(quadrant == 2){
                    setWheelPower(1, varWheels, varWheels, 1, radius);
                }
                else if(quadrant == 3){
                    setWheelPower(-varWheels, -1, -1, -varWheels, radius);
                }
                else if(quadrant == 4){
                    setWheelPower(-1, -varWheels, -varWheels, -1, radius);
                }
            }
        }

        checkGlyphClaw();

        telemetry.addData("w1: ", w1.getPower());
        telemetry.addData("w2: ", w2.getPower());
        telemetry.addData("w3: ", w3.getPower());
        telemetry.addData("w4: ", w4.getPower());
        //telemetry.addData("Quadrant: ", quadrant);
        //telemetry.addData("RotX: ", rotX);
        //telemetry.addData("Angle: ", angle);
        telemetry.addData("Claw Pos: ", glyphClaw.getPosition());
        telemetry.addData("A Pressed: ", aPressed);*/
    }

    /*public void checkGlyphClaw(){
        if(gamepad1.a){
            if(!aPressed){
                aPressed = true;
                if(glyphClawPosition == GLYPH_CLAW_OPEN_POSITION){
                    glyphClaw.setPosition(GLYPH_CLAW_CLOSE_POSITION);
                    glyphClawPosition = GLYPH_CLAW_CLOSE_POSITION;
                }
                else if(glyphClawPosition == GLYPH_CLAW_CLOSE_POSITION){
                    glyphClaw.setPosition(GLYPH_CLAW_OPEN_POSITION);
                    glyphClawPosition = GLYPH_CLAW_OPEN_POSITION;
                }
            }
        }
        else{
            aPressed = false;
        }
    }*/

    /*public void dropLeft(){
        timer.reset();
        dropLeft.setPosition(0.575);
        while(timer.seconds() < 1){

        }
        timer.reset();
        rotationLeft.setPosition(0.45);
        while(timer.seconds() < 1){

        }
        timer.reset();
        dropLeft.setPosition(.3);
        while(timer.seconds() < 1){

        }
        rotationLeft.setPosition(0.525);
        timer.reset();
        while(timer.seconds() < 1){

        }
        dropLeft.setPosition(0);
    }

    public void dropRight(){
        timer.reset();
        dropRight.setPosition(.7);
        while(timer.seconds() < 1){

        }
        timer.reset();
        rotationRight.setPosition(0.45);
        while(timer.seconds() < 1){

        }
        dropRight.setPosition(1);
    }*/

    public void raiseRight(){

    }

    public void raiseLeft(){

    }

    public void glyphLiftToPos2(){
        int counts = (int)glyphLiftInchesToCounts(7);
        glyphLift.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        glyphLift.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        glyphLift.setTargetPosition(counts);
        glyphLift.setPower(.15);
        while(Math.abs(-glyphLift.getCurrentPosition() - counts) > DELTA){
            telemetry.addData("Lift Height: ", -glyphLiftCountsToInches(glyphLift.getCurrentPosition()));
            telemetry.update();

            if(-glyphLift.getCurrentPosition() > counts){
                break;
            }
        }

        glyphLift.setTargetPosition(glyphLift.getCurrentPosition());
        glyphLift.setPower(0);
    }

    public void stopGlyphLift(){
        glyphLift.setTargetPosition(glyphLift.getCurrentPosition());
        glyphLift.setPower(0);
    }

    public int glyphLiftInchesToCounts(double inches){
        return (int)(inches * GLYPH_LIFT_COUNTS_PER_INCH);
    }

    public double glyphLiftCountsToInches(int counts){
        return counts / GLYPH_LIFT_COUNTS_PER_INCH;
    }

    /*public void setWheelPower(double p1, double p2, double p3, double p4, double radius){
        w1.setPower(p1 * radius / 2);
        w2.setPower(p2 * radius / 2);
        w3.setPower(p3 * radius / 2 );
        w4.setPower(p4 * radius / 2);
    }*/
}
