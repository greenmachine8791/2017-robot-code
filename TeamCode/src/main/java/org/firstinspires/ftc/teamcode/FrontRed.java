package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.util.ElapsedTime;

/**
 * Created by ifwel on 2/1/2018.
 */

@Autonomous(name="Front Red")
public class FrontRed extends Robot {

    ElapsedTime timer = new ElapsedTime();
    String vuMarkPosition;

    public void runOpMode(){
        initialize();

        waitForStart();

        claws.grab();

        jb.dropRight();

        timer.reset();

        while(timer.seconds() < 1.5){
            telemetry.addData("Color: ", cs.getRightColor());
            telemetry.addData("Hue: ", cs.hsvRight[0]);
            telemetry.addData("Red: ", cs.rightSensor.red());
            telemetry.addData("Blue: ", cs.rightSensor.blue());
            vuMarkPosition = pc.getVuMarkPosition();
            telemetry.addData("Glyph Position", vuMarkPosition);
            telemetry.update();
        }

        knockOffJewel("red");

        jb.raiseRight();

        lifts.glyphLiftToOneInch();

        if(vuMarkPosition.equals("RIGHT") || vuMarkPosition.equals("UNKNOWN")){
            dt.driveFoward(24);

            dt.resetEncoders();

            dt.drive(1000, -1000, 1000, -1000, 0.8);

            dt.resetEncoders();

            dt.driveFoward(10);

            dt.resetEncoders();

            claws.release();

            dt.driveBackward(2.5);

            dt.resetEncoders();
        }
        else if(vuMarkPosition.equals("CENTER")){
            dt.driveFoward(28.);

            dt.resetEncoders();

            dt.drive(900, -900, 900, -900, 0.8);

            dt.resetEncoders();

            dt.driveFoward(10);

            dt.resetEncoders();

            claws.release();

            dt.driveBackward(2.5);

            dt.resetEncoders();
        }
        else if(vuMarkPosition.equals("LEFT")){
            dt.driveFoward(34);

            dt.resetEncoders();

            dt.drive(900, -900, 900, -900, 0.8);

            dt.resetEncoders();

            dt.driveFoward(10);

            dt.resetEncoders();

            claws.release();

            dt.driveBackward(2.5);

            dt.resetEncoders();
        }
        
        lifts.glyphLiftToPos1();
    }
}
