package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.util.ElapsedTime;

/**
 * Created by ifwel on 2/7/2018.
 */

@Autonomous(name="Front Blue")
public class FrontBlue extends Robot {


    ElapsedTime timer = new ElapsedTime();
    String vuMarkPosition;

    public void runOpMode(){
        initialize();

        waitForStart();

        claws.grab();

        jb.dropLeft();

        timer.reset();

        while(timer.seconds() < 1.5){
            telemetry.addData("Color: ", cs.getLeftColor());
            telemetry.addData("Hue: ", cs.hsvLeft[0]);
            telemetry.addData("Red: ", cs.leftSensor.red());
            telemetry.addData("Blue: ", cs.leftSensor.blue());
            vuMarkPosition = pc.getVuMarkPosition();
            telemetry.addData("Glyph Position", vuMarkPosition);
            telemetry.update();
        }

        knockOffJewel("blue");

        jb.raiseLeft();

        lifts.glyphLiftToOneInch();

        if(vuMarkPosition.equals("LEFT") || vuMarkPosition.equals("UNKNOWN")){
            dt.driveFoward(26.5);

            dt.resetEncoders();

            dt.drive(-900, 900, -900, 900, 0.8);

            dt.resetEncoders();

            dt.driveFoward(10);

            dt.resetEncoders();

            claws.release();

            dt.driveBackward(2.5);

            dt.resetEncoders();
        }
        else if(vuMarkPosition.equals("CENTER")){
            dt.driveFoward(31);

            dt.resetEncoders();

            dt.drive(-775, 775, -775, 775, 0.8);

            dt.resetEncoders();

            dt.driveFoward(10);

            dt.resetEncoders();

            claws.release();

            dt.driveBackward(2.5);

            dt.resetEncoders();
        }
        else if(vuMarkPosition.equals("RIGHT")){
            dt.driveFoward(37);

            dt.resetEncoders();

            dt.drive(-750, 750, -750, 750, 0.8);

            dt.resetEncoders();

            dt.driveFoward(10);

            dt.resetEncoders();

            claws.release();

            dt.driveBackward(2.5);

            dt.resetEncoders();
        }

        lifts.glyphLiftToPos1();
    }
}
