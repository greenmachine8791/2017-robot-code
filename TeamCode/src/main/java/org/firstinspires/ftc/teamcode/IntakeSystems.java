package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.DcMotor;

/**
 * Created by ifwel on 12/28/2017.
 */

public class IntakeSystems{
    public DcMotor glyphIntake;

    public double glyphIntakePower = 0.75;

    public IntakeSystems(DcMotor glyphIntake){
        this.glyphIntake = glyphIntake;
    }

    public void stopGlyphIntake(){
        glyphIntake.setPower(0);
    }

    public void glyphIntakeIn(){
        glyphIntake.setPower(glyphIntakePower);

    }

    public void glyphIntakeOut(){
        glyphIntake.setPower(-glyphIntakePower);
    }
}
