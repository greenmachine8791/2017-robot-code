package org.firstinspires.ftc.teamcode;

import org.firstinspires.ftc.robotcontroller.external.samples.ConceptVuforiaNavigation;
import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.matrices.OpenGLMatrix;
import org.firstinspires.ftc.robotcore.external.matrices.VectorF;
import org.firstinspires.ftc.robotcore.external.navigation.AngleUnit;
import org.firstinspires.ftc.robotcore.external.navigation.AxesOrder;
import org.firstinspires.ftc.robotcore.external.navigation.AxesReference;
import org.firstinspires.ftc.robotcore.external.navigation.Orientation;
import org.firstinspires.ftc.robotcore.external.navigation.RelicRecoveryVuMark;
import org.firstinspires.ftc.robotcore.external.navigation.VuMarkInstanceId;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackable;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackableDefaultListener;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackables;

/**
 * Created by ifwel on 12/28/2017.
 */

public class PhoneCamera {
    VuforiaLocalizer vuforia;
    VuforiaTrackables relicTrackables;
    VuforiaTrackable relicTemplate;
    VuforiaLocalizer.Parameters parameters;
    RelicRecoveryVuMark vuMark;

    int cameraMonitorViewId;

    public PhoneCamera(int cameraMonitorViewId){
        this.cameraMonitorViewId = cameraMonitorViewId;

        parameters = new VuforiaLocalizer.Parameters(cameraMonitorViewId);

        parameters.vuforiaLicenseKey = "ARjc9o3/////AAAAGUJj4R4rQ04RrzpKopl+6MAA5J39vcwChSoErgeiiDvXLkFqGfpA4PEBB6kpOvo+dFqVcKOTpQwtVspwL8slu5QGljc/WAElyr2i97Wg8ps1i4LEaynaDze+/NFBsK8W5iAcAzfOtwxiAuTfpjsOzxnkBb6dci8q0Ww0K5kUzWnTcWliAx6k59gK0bgc8/h+hwWSfT3P9nkSJdppRSAw/gJ9FpbrIhu9dLTcKTWndq1JqgoRHGVSBnHmOv3RLY8Nx6Qbxr9W4g9AIwvpuXXyAd7ilpE5PZ++W/1dej72yEyhE9gavlq08C/fcsE0ND2KOpbFMlqP4Af4IRPhPGGR6Ifeis5iJ/I8Dj9Q0PZ2glvn";

        parameters.cameraDirection = VuforiaLocalizer.CameraDirection.BACK;
        this.vuforia = ClassFactory.createVuforiaLocalizer(parameters);

        relicTrackables = this.vuforia.loadTrackablesFromAsset("RelicVuMark");
        relicTemplate = relicTrackables.get(0);
        relicTemplate.setName("relicVuMarkTemplate");

        relicTrackables.activate();

        vuMark = RelicRecoveryVuMark.from(relicTemplate);
    }

    public String getVuMarkPosition(){
        vuMark = RelicRecoveryVuMark.from(relicTemplate);
        return vuMark.toString();
    }
}
