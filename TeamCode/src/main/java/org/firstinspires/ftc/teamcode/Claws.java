package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.hardware.Servo;

/**
 * Created by ifwel on 11/13/2017.
 */

public class Claws {
    public final double GLYPH_CLAW_START_POSITION = 0.87;
    public final double GLYPH_CLAW_CLOSE_POSITION = 0.3;
    public final double GLYPH_CLAW_OPEN_POSITION = 0.6;

    public final double RELIC_CLAW_START_POSITION = .95;
    public final double RELIC_CLAW_OPEN_POSITION = 0.5;
    //public final double RELIC_CLAW_INT_POSITION = 0.25;
    public final double RELIC_CLAW_CLOSE_POSITION = 0.03;

    public Servo glyphClaw;
    public Servo relicClaw;

    public Claws(Servo glyphClaw, Servo relicClaw){
        this.glyphClaw = glyphClaw;
        this.glyphClaw.setPosition(GLYPH_CLAW_START_POSITION);

        this.relicClaw = relicClaw;
        this.relicClaw.setPosition(RELIC_CLAW_START_POSITION);
    }

    public void grab(){
        glyphClaw.setPosition(GLYPH_CLAW_CLOSE_POSITION);
    }

    public void release(){
        glyphClaw.setPosition(GLYPH_CLAW_OPEN_POSITION);
    }

    public double getGlyphClawPosition(){
        return glyphClaw.getPosition();
    }
}
