package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.navigation.RelicRecoveryVuMark;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackable;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaTrackables;

/**
 * Created by ifwel on 12/15/2017.
 */

@Autonomous(name="Back Red")
public class BackRed extends Robot{

    ElapsedTime timer = new ElapsedTime();
    String vuMarkPosition;

    public void runOpMode(){
        initialize();

        waitForStart();

        claws.grab();

        jb.dropRight();

        timer.reset();

        while(timer.seconds() < 1.5){
            telemetry.addData("Color: ", cs.getRightColor());
            telemetry.addData("Hue: ", cs.hsvRight[0]);
            telemetry.addData("Red: ", cs.rightSensor.red());
            telemetry.addData("Blue: ", cs.rightSensor.blue());
            vuMarkPosition = pc.getVuMarkPosition();
            telemetry.addData("Glyph Position", vuMarkPosition);
            telemetry.update();
        }

        knockOffJewel("red");

        jb.raiseRight();

        lifts.glyphLiftToOneInch();

        dt.driveFoward(24);

        dt.resetEncoders();

        if(vuMarkPosition.equals("LEFT")){
            dt.driveFoward(2);

            dt.resetEncoders();

            dt.drive(-850, 850,-850, 850, 0.8);

            dt.resetEncoders();

            dt.driveFoward(13);

            dt.resetEncoders();

            dt.drive(450, -450,450, -450, 0.8);

            dt.resetEncoders();

            dt.driveFoward(3);

            dt.resetEncoders();

            claws.release();

            dt.driveBackward(2.5);

            dt.resetEncoders();
        }
        else if(vuMarkPosition.equals("RIGHT") || vuMarkPosition.equals("UNKNOWN")){
            dt.drive(-275, 275, -275, 275, .8);

            dt.resetEncoders();

            dt.driveFoward(10);

            dt.resetEncoders();

            claws.release();

            dt.driveBackward(2.5);

            dt.resetEncoders();
        }
        else if(vuMarkPosition.equals("CENTER")){
            dt.drive(-550, 550, -550, 550, 0.8);

            dt.resetEncoders();

            dt.driveFoward(11.5);

            dt.resetEncoders();

            claws.release();

            dt.driveBackward(2.5);

            dt.resetEncoders();
        }

        lifts.glyphLiftToPos1();
    }
}
