package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;

/**
 * Created by ifwel on 11/1/2017.  useless comment dwdwdee
 */

public class JewelBars {
    public ElapsedTime timer = new ElapsedTime();

    public Servo dropRight;
    public Servo dropLeft;
    public Servo rotationRight;
    public Servo rotationLeft;

    public final double DR_START_POSITION = 0.3;
    public final double DL_START_POSITION = 0.6;
    public final double RR_START_POSITION = 0.7;
    public final double RL_START_POSITION = 0.5;

    public JewelBars(Servo dropRight, Servo dropLeft, Servo rotationRight, Servo rotationLeft){
        this.dropRight = dropRight;
        this.dropLeft = dropLeft;
        this.rotationRight = rotationRight;
        this.rotationLeft = rotationLeft;

        //Test Push From Jonah
        dropRight.setPosition(DR_START_POSITION);
        dropLeft.setPosition(DL_START_POSITION);
        rotationRight.setPosition(RR_START_POSITION);
        rotationLeft.setPosition(RL_START_POSITION);
    }

    public void dropRight(){
        timer.reset();
        dropRight.setPosition(.7);
        while(timer.milliseconds() < 500){

        }
        timer.reset();
        rotationRight.setPosition(0.45);
        while(timer.milliseconds() < 500){

        }
        timer.reset();
        dropRight.setPosition(1);
        while(timer.milliseconds() < 500){

        }
        /*timer.reset();
        rotationRight.setPosition(.43);
        while(timer.milliseconds() < 500){

        }*/
    }

    public void dropLeft(){
        timer.reset();
        dropLeft.setPosition(0.575);
        while(timer.milliseconds() < 500){

        }
        timer.reset();
        rotationLeft.setPosition(0.45);
        while(timer.milliseconds() < 500){

        }
        timer.reset();
        dropLeft.setPosition(.3);
        while(timer.milliseconds() < 500){

        }
        timer.reset();
        rotationLeft.setPosition(0.525);
        while(timer.milliseconds() < 500){

        }
        timer.reset();
        dropLeft.setPosition(0);
        while(timer.milliseconds() < 500){

        }
        /*timer.reset();
        rotationLeft.setPosition(0.56);
        while(timer.milliseconds() < 500){

        }*/
    }

    public void raiseLeft(){
        timer.reset();
        dropLeft.setPosition(.3);
        while(timer.milliseconds() < 500){

        }
        timer.reset();
        rotationLeft.setPosition(0.45);
        while(timer.milliseconds() < 500){

        }
        timer.reset();
        dropLeft.setPosition(0.575);
        while(timer.milliseconds() < 500){

        }
        timer.reset();
        rotationLeft.setPosition(RL_START_POSITION);
        while(timer.milliseconds() < 500){

        }
        timer.reset();
        dropLeft.setPosition(DL_START_POSITION);
        while(timer.milliseconds() < 500){

        }
    }

    public void raiseRight(){
        timer.reset();
        dropRight.setPosition(0.7);
        while(timer.milliseconds() < 500){

        }
        timer.reset();
        rotationRight.setPosition(RR_START_POSITION);
        while(timer.milliseconds() < 500){

        }
        timer.reset();
        dropRight.setPosition(DR_START_POSITION);
        while(timer.milliseconds() < 500){

        }
    }

    public void moveDropRight(double change){
        dropRight.setPosition(dropRight.getPosition() + change);
    }

    public void moveDropLeft(double change){
        dropLeft.setPosition(dropLeft.getPosition() + change);
    }

    public void moveRotationRight(double change){
        rotationRight.setPosition(rotationRight.getPosition() + change);
    }

    public void moveRotationLeft(double change){
        rotationLeft.setPosition(rotationLeft.getPosition() + change);
    }
}
