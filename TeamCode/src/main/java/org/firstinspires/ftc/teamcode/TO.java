package org.firstinspires.ftc.teamcode;

import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.CRServo;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.util.ElapsedTime;

/**
 * Created by ifwel on 12/8/2017.
 */

@TeleOp(name="TeleOp")
public class TO extends OpMode{
    public ElapsedTime timer = new ElapsedTime();

    public final double GLYPH_LIFT_GEAR_DIAMETER_INCHES = 1.135;
    public final int GLYPH_LIFT_COUNTS_PER_REVOLUTION = 140;
    public final double GLYPH_LIFT_INCHES_PER_REVOLUTION = (3.141 * GLYPH_LIFT_GEAR_DIAMETER_INCHES / 4)* 1.12;
    public final double GLYPH_LIFT_COUNTS_PER_INCH = GLYPH_LIFT_COUNTS_PER_REVOLUTION/GLYPH_LIFT_INCHES_PER_REVOLUTION;
    public final int DELTA = 5;

    public final double GLYPH_CLAW_CLOSE_POSITION = 0.3;
    public final double GLYPH_CLAW_OPEN_POSITION = 0.6;

    public final double RELIC_CLAW_START_POSITION = .95;
    public final double RELIC_CLAW_OPEN_POSITION = 0.5;
    //public final double RELIC_CLAW_INT_POSITION = 0.25;
    public final double RELIC_CLAW_CLOSE_POSITION = 0.03;

    public final int RELIC_EXTENSION_COUNT_LIMIT = 6200;
    public final int RELIC_LIFT_COUNT_LIMIT = 5200;

    public final double DR_START_POSITION = 0.3;
    public final double DL_START_POSITION = 0.6;
    public final double RR_START_POSITION = 0.7;
    public final double RL_START_POSITION = 0.5;

    public final double FAST_POWER_FACTOR = 1.3;
    public final double INT_POWER_FACTOR = 2.0;
    public final double SLOW_POWER_FACTOR = 3.2;

    public final String GLYPH_INTAKE_STOPPED = "stopped";
    public final String GLYPH_INTAKE_IN = "in";
    public final String GLYPH_INTAKE_OUT = "out";

    public DcMotor w1;
    public DcMotor w2;
    public DcMotor w3;
    public DcMotor w4;

    public DcMotor glyphLift;
    public DcMotor glyphIntake;
    public Servo glyphClaw;

    public Servo dropRight;
    public Servo dropLeft;
    public Servo rotationRight;
    public Servo rotationLeft;

    public DcMotor relicLift;
    public DcMotor relicExtension;
    public Servo relicClaw;

    public double glyphLiftPower;
    public double glyphIntakePower;
    public double glyphClawPosition;

    public double relicLiftPower;
    public double relicExtensionPower;
    public double relicClawPosition;

    public double powerFactor;
    public int position;

    public boolean aPressed;
    public boolean bPressed;
    public boolean yPressed;
    public boolean xPressed;
    public boolean startPressed;

    public String glyphIntakeDirection;

    public void init(){
        w1 = hardwareMap.dcMotor.get("w1");
        w2 = hardwareMap.dcMotor.get("w2");
        w3 = hardwareMap.dcMotor.get("w3");
        w4 = hardwareMap.dcMotor.get("w4");

        w1.setDirection(DcMotorSimple.Direction.REVERSE);
        w4.setDirection(DcMotorSimple.Direction.REVERSE);

        w1.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        w2.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        w3.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        w4.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        w1.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        w2.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        w3.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        w4.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        glyphLift = hardwareMap.dcMotor.get("glyphLift");
        glyphIntake = hardwareMap.dcMotor.get("glyphIntake");
        glyphClaw = hardwareMap.servo.get("glyphClaw");

        glyphLift.setDirection(DcMotorSimple.Direction.REVERSE);
        glyphLift.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        glyphLift.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        glyphIntake.setDirection(DcMotorSimple.Direction.REVERSE);
        glyphClaw.setPosition(GLYPH_CLAW_OPEN_POSITION);
        glyphClawPosition = GLYPH_CLAW_CLOSE_POSITION;

        dropRight = hardwareMap.servo.get("dropRight");
        dropLeft = hardwareMap.servo.get("dropLeft");
        rotationRight = hardwareMap.servo.get("rotationRight");
        rotationLeft = hardwareMap.servo.get("rotationLeft");

        dropRight.setPosition(DR_START_POSITION);
        dropLeft.setPosition(DL_START_POSITION);
        rotationRight.setPosition(RR_START_POSITION);
        rotationLeft.setPosition(RL_START_POSITION);

        relicLift = hardwareMap.dcMotor.get("relicLift");
        relicExtension = hardwareMap.dcMotor.get("relicExtension");
        relicClaw = hardwareMap.servo.get("relicClaw");

        relicLift.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        relicLift.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
        relicExtension.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        relicExtension.setMode(DcMotor.RunMode.RUN_USING_ENCODER);

        relicClawPosition = RELIC_CLAW_START_POSITION;
        relicClaw.setPosition(RELIC_CLAW_START_POSITION);

        glyphIntakeDirection = GLYPH_INTAKE_STOPPED;

        glyphLiftPower = .75;
        glyphIntakePower = .75;

        relicLiftPower = 0.35;
        relicExtensionPower = 0.5;

        powerFactor = INT_POWER_FACTOR;
        position = 1;

        aPressed = false;
        bPressed = false;
        yPressed = false;
        xPressed = false;
        startPressed = false;
    }

    public void loop(){
        slowPowerToggle();
        wheels();
        //glyphClaw();
        glyphLift();
        relicLift();
        relicExtension();
        relicClaw();
        glyphIntake();

        telemetry.addData("Lift Position: ", position);
        telemetry.addData("Relic Lift", relicLift.getCurrentPosition());
        telemetry.addData("Relic Extension", relicExtension.getCurrentPosition());
    }

    public void wheels(){
        double x = -gamepad1.left_stick_x;
        double y = gamepad1.left_stick_y;
        double rotX = -gamepad1.right_stick_x;
        boolean rotating = false;
        double radius = Math.sqrt((x * x) + (y * y));
        double angle = Math.abs(Math.toDegrees(Math.atan(y/x)));
        double varWheels = angle/45 - 1;
        int quadrant = 0;

        if(Math.abs(rotX) > 0.03){
            rotating = true;

            if(rotX > 0){
                setWheelFullPower(1, -1, 1, -1, rotX);
            }
            else{
                setWheelFullPower(-1, 1, -1, 1, -rotX);
            }
        }


        if(!rotating){
            if(Math.abs(x) < .03 && Math.abs(y) < .03){
                setWheelPower(0, 0, 0, 0, radius);
            }
            else if(angle >= 80){
                setWheelPower(y, y, y, y, 1);
            }
            else if(angle <= 10){
                if(x > 0){
                    setWheelPower(-1, 1, 1, -1, radius);
                }
                else if(x < 0){
                    setWheelPower(1, -1, -1, 1, radius);
                }
            }
            else{
                if(x > 0 && y > 0){
                    quadrant = 1;
                }
                else if(x < 0 && y > 0){
                    quadrant = 2;
                }
                else if(x < 0 && y < 0){
                    quadrant = 3;
                }
                else if(x > 0 && y < 0){
                    quadrant = 4;
                }

                if(quadrant == 0){
                    setWheelPower(0, 0, 0, 0, radius);
                }
                else if(quadrant == 1){
                    setWheelPower(varWheels, 1, 1, varWheels, radius);
                }
                else if(quadrant == 2){
                    setWheelPower(1, varWheels, varWheels, 1, radius);
                }
                else if(quadrant == 3){
                    setWheelPower(-varWheels, -1, -1, -varWheels, radius);
                }
                else if(quadrant == 4){
                    setWheelPower(-1, -varWheels, -varWheels, -1, radius);
                }
            }
        }
    }

    public void glyphLift(){
        if(gamepad1.right_bumper){
            glyphLiftToPos2();
        }

        if(gamepad1.right_trigger > .5){
            glyphLiftToPos3();
        }

        if(gamepad1.left_trigger > .5){
            glyphLiftToPos1();
        }
    }

    public void glyphLiftToPos1(){
        int counts = glyphLiftInchesToCounts(0);
        position = 1;
        glyphLift.setTargetPosition(counts);
        glyphLift.setPower(glyphLiftPower);
        timer.reset();
    }

    public void glyphLiftToPos2(){
        int counts = (int)glyphLiftInchesToCounts(7);
        position = 2;
        glyphLift.setTargetPosition(counts);
        glyphLift.setPower(glyphLiftPower);
        timer.reset();
    }

    public void glyphLiftToPos3(){
        int counts = (int)glyphLiftInchesToCounts(12.9);
        position = 3;
        glyphLift.setTargetPosition(counts);
        glyphLift.setPower(glyphLiftPower);
        timer.reset();
    }

    //Don't use anymore, keeping it around just in case
    public void glyphLiftToOneInch(){
        int counts = (int)glyphLiftInchesToCounts(1.5);
        position = 4;
        glyphLift.setTargetPosition(counts);
        glyphLift.setPower(glyphLiftPower);
        timer.reset();
    }

    //Soon to be deprecated once the intake system is built, but still in use for now
    public void glyphClaw(){
        if(gamepad1.a){
            if(!aPressed){
                aPressed = true;
                if(glyphClawPosition == GLYPH_CLAW_OPEN_POSITION){
                    closeGlyphClaw();
                }
                else if(glyphClawPosition == GLYPH_CLAW_CLOSE_POSITION){
                    openGlyphClaw();
                }
            }
        }
        else{
            aPressed = false;
        }
    }

    public void glyphIntake(){
        if(gamepad1.a){
            if(!aPressed && !bPressed){
                aPressed = true;
                if(glyphIntakeDirection.equals(GLYPH_INTAKE_IN)){
                    glyphIntake.setPower(0);
                    glyphIntakeDirection = GLYPH_INTAKE_STOPPED;
                }
                else if(glyphIntakeDirection.equals(GLYPH_INTAKE_OUT) ||
                        glyphIntakeDirection.equals(GLYPH_INTAKE_STOPPED)){
                    glyphIntake.setPower(glyphIntakePower);
                    glyphIntakeDirection = GLYPH_INTAKE_IN;
                }
            }
        }
        else{
            aPressed = false;
        }

        if(gamepad1.b){
            if(!aPressed && !bPressed){
                bPressed = true;
                if(glyphIntakeDirection.equals(GLYPH_INTAKE_OUT)){
                    glyphIntake.setPower(0);
                    glyphIntakeDirection = GLYPH_INTAKE_STOPPED;
                }
                else if(glyphIntakeDirection.equals(GLYPH_INTAKE_IN) ||
                        glyphIntakeDirection.equals(GLYPH_INTAKE_STOPPED)){
                    glyphIntake.setPower(-glyphIntakePower);
                    glyphIntakeDirection = GLYPH_INTAKE_OUT;
                }
            }
        }
        else{
            bPressed = false;
        }
    }

    public void relicLift(){
        if((gamepad2.dpad_up || gamepad1.dpad_up) && relicLift.getCurrentPosition() < RELIC_LIFT_COUNT_LIMIT){
            relicLift.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            relicLift.setPower(relicLiftPower);
        }
        else if((gamepad2.dpad_down || gamepad1.dpad_down) && relicLift.getCurrentPosition() > 0){
            relicLift.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            relicLift.setPower(-relicLiftPower);
        }
        else if(gamepad1.left_bumper || gamepad2.b){
            relicLiftToOneInch();
        }
        else{
            relicLift.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            relicLift.setTargetPosition(relicLift.getCurrentPosition());
            relicLift.setPower(0.5);
        }
    }

    public void relicExtension(){
        if(gamepad2.right_bumper && relicExtension.getCurrentPosition() < RELIC_EXTENSION_COUNT_LIMIT){
            relicExtension.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            relicExtension.setPower(relicExtensionPower);
        }
        else if(gamepad2.left_bumper && relicExtension.getCurrentPosition() > 0){
            relicExtension.setMode(DcMotor.RunMode.RUN_USING_ENCODER);
            relicExtension.setPower(-relicExtensionPower);
        }
        else{
            relicExtension.setMode(DcMotor.RunMode.RUN_TO_POSITION);
            relicExtension.setTargetPosition(relicExtension.getCurrentPosition());
            relicExtension.setPower(0.2);
        }
    }

    public void relicLiftToOneInch(){
        relicLift.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        relicLift.setTargetPosition(glyphLiftInchesToCounts(5)); // Check this out later
        relicLift.setPower(relicLiftPower);
        while(relicLift.isBusy() && Math.abs(relicLift.getCurrentPosition() - glyphLiftInchesToCounts(1.5)) > 10){

        }
        relicLift.setPower(relicLiftPower);
    }

    public void relicClaw(){
        if(gamepad1.x || gamepad2.x){
            if(!xPressed){
                xPressed = true;
                if(relicClawPosition == RELIC_CLAW_CLOSE_POSITION ||
                        relicClawPosition == RELIC_CLAW_START_POSITION){
                    relicClaw.setPosition(RELIC_CLAW_OPEN_POSITION);
                    relicClawPosition = RELIC_CLAW_OPEN_POSITION;
                }
                else if(relicClawPosition == RELIC_CLAW_OPEN_POSITION){
                    relicClaw.setPosition(RELIC_CLAW_CLOSE_POSITION);
                    relicClawPosition = RELIC_CLAW_CLOSE_POSITION;
                }
                /*else if(relicClawPosition == RELIC_CLAW_INT_POSITION){
                    relicClaw.setPosition(RELIC_CLAW_CLOSE_POSITION);
                    relicClawPosition = RELIC_CLAW_CLOSE_POSITION;
                }*/
            }
        }
        else{
            xPressed = false;
        }
    }

    //Don't use, but keeping it around just in case
    public void slowerPowerToggle(){
        if(gamepad1.start){
            if(!startPressed){
                startPressed = true;
                if(powerFactor == FAST_POWER_FACTOR || powerFactor == SLOW_POWER_FACTOR){
                    powerFactor = INT_POWER_FACTOR;
                }
                else if(powerFactor == INT_POWER_FACTOR){
                    powerFactor = FAST_POWER_FACTOR;
                }
            }
        }
        else{
            startPressed = false;
        }
    }

    public void slowPowerToggle(){
        if(gamepad1.y){
            if(!yPressed) {
                yPressed = true;
                if (powerFactor == INT_POWER_FACTOR) {
                    powerFactor = SLOW_POWER_FACTOR;
                }
                else if (powerFactor == SLOW_POWER_FACTOR) {
                    powerFactor = INT_POWER_FACTOR;
                }
            }
        }
        else{
            yPressed = false;
        }
    }

    public void setWheelPower(double p1, double p2, double p3, double p4, double radius){
        w1.setPower(p1 * radius / powerFactor);
        w2.setPower(p2 * radius / powerFactor);
        w3.setPower(p3 * radius / powerFactor);
        w4.setPower(p4 * radius / powerFactor);
    }

    public void setWheelFullPower(double p1, double p2, double p3, double p4, double radius){
        w1.setPower(p1 * radius);
        w2.setPower(p2 * radius);
        w3.setPower(p3 * radius);
        w4.setPower(p4 * radius);
    }

    public void stopGlyphLift(){
        glyphLift.setTargetPosition(glyphLift.getCurrentPosition());
        glyphLift.setPower(0);
    }

    public int glyphLiftInchesToCounts(double inches){
        return (int)(inches * GLYPH_LIFT_COUNTS_PER_INCH);
    }

    public double glyphLiftCountsToInches(int counts){
        return counts / GLYPH_LIFT_COUNTS_PER_INCH;
    }

    public void closeGlyphClaw(){
        glyphClaw.setPosition(GLYPH_CLAW_CLOSE_POSITION);
        glyphClawPosition = GLYPH_CLAW_CLOSE_POSITION;
    }

    public void openGlyphClaw(){
        glyphClaw.setPosition(GLYPH_CLAW_OPEN_POSITION);
        glyphClawPosition = GLYPH_CLAW_OPEN_POSITION;
    }
}
